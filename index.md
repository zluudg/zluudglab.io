---
title: Zluudg's Home Page
layout: default
---
Hello!

I'm a dev specializing in software for telco systems, high and low. Big and
small. Embedded C develoment for switches? Check. Backend coding for messaging
services? Check. Deploying and testing network management systems? Check.
Hardening of protocol implementations? Check.

Feel free to [ping me]({{ "contact" | relative_url }}) if you're curious to know more about my work.

Cheers!

![me]({{ "/assets/profile_animated_short.gif" | relative_url }})
