#!/bin/bash

# the "getcert" used below is a function that I've added to my .bash_profile
# like so:
# getcert() {
#     openssl s_client -connect $1:443 < /dev/null | openssl x509 -text
# }
# export -f getcert

getcert vector.im > p1-a 2>&1
getcert element.io > p1-b 2>&1
if [[ $(diff p1-a p1-b) ]]; then
    echo "vector.im and element.io - DIFF"
else
    echo "vector.im and element.io - SAME"
fi

getcert riot.im > p2-a 2>&1
getcert element.io > p2-b 2>&1
if [[ $(diff p2-a p2-b) ]]; then
    echo "riot.im and element.io - DIFF"
else
    echo "riot.im and element.io - SAME"
fi

getcert gogle.com > p3-a 2>&1
getcert www.google.com > p3-b 2>&1
if [[ $(diff p3-a p3-b) ]]; then
    echo "gogle.com and www.google.com - DIFF"
else
    echo "gogle.com and www.google.com - SAME"
fi

getcert gooogle.com > p4-a 2>&1
getcert www.google.com > p4-b 2>&1
if [[ $(diff p4-a p4-b) ]]; then
    echo "gooogle.com and www.google.com - DIFF"
else
    echo "gooogle.com and www.google.com - SAME"
fi

getcert www.sprint.com > p5-a 2>&1
getcert www.t-mobile.com > p5-b 2>&1
if [[ $(diff p5-a p5-b) ]]; then
    echo "www.sprint.com and www.t-mobile.com - DIFF"
else
    echo "www.sprint.com and www.t-mobile.com - SAME"
fi

getcert x.com > p6-a 2>&1
getcert twitter.com > p6-b 2>&1
if [[ $(diff p6-a p6-b) ]]; then
    echo "x.com and twitter.com - DIFF"
else
    echo "x.com and twitter.com - SAME"
fi

getcert t-mobile.com > p7-a 2>&1
getcert www.t-mobile.com > p7-b 2>&1
if [[ $(diff p7-a p7-b) ]]; then
    echo "t-mobile.com and www.t-mobile.com - DIFF"
else
    echo "t-mobile.com and www.t-mobile.com - SAME"
fi

getcert google.com > p8-a 2>&1
getcert www.google.com > p8-b 2>&1
if [[ $(diff p8-a p8-b) ]]; then
    echo "google.com and www.google.com - DIFF"
else
    echo "google.com and www.google.com - SAME"
fi

getcert apple.com > p9-a 2>&1
getcert www.apple.com > p9-b 2>&1
if [[ $(diff p9-a p9-b) ]]; then
    echo "apple.com and www.apple.com - DIFF"
else
    echo "apple.com and www.apple.com - SAME"
fi

getcert gogle.com > p10-a 2>&1
getcert google.com > p10-b 2>&1
if [[ $(diff p10-a p10-b) ]]; then
    echo "gogle.com and google.com - DIFF"
else
    echo "gogle.com and google.com - SAME"
fi

getcert gooogle.com > p11-a 2>&1
getcert google.com > p11-b 2>&1
if [[ $(diff p11-a p11-b) ]]; then
    echo "gooogle.com and google.com - DIFF"
else
    echo "gooogle.com and google.com - SAME"
fi
