#!/bin/bash
curl -v http://vector.im > http-vector-im 2>&1
curl -v https://vector.im > https-vector-im 2>&1
curl -v http://riot.im > http-riot-im 2>&1
curl -v https://riot.im > https-riot-im 2>&1

curl -v http://www.gogle.com > http-www-gogle-com 2>&1
curl -v https://www.gogle.com > https-www-gogle-com 2>&1
curl -v http://www.gooogle.com > http-www-gooogle-com 2>&1
curl -v https://www.gooogle.com > https-www-gooogle-com 2>&1

curl -v http://sprint.com > http-sprint-com 2>&1
curl -v https://sprint.com > https-sprint-com 2>&1
curl -v http://www.sprint.com > http-www-sprint-com 2>&1
curl -v https://www.sprint.com > https-www-sprint-com 2>&1

curl -v http://t-mobile.com > http-t-mobile-com 2>&1
curl -v https://t-mobile.com > https-t-mobile-com 2>&1
curl -v http://google.com > http-google-com 2>&1
curl -v https://google.com > https-google-com 2>&1

curl -v http://x.com > http-x-com 2>&1
curl -v https://x.com > https-x-com 2>&1
