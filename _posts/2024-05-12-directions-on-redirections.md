---
layout: post
---

# Directions on Redirections
So, I'm now a proud member of the exclusive
people-who-own-2LDs-with-five-characters-or-less club. Nothing big, just me,
Elon Musk, Jon Postel, whoever is in charge of the X Window system and some
other peepz. However, I don't really intend to serve anything under my "z5.nu"
domain directly. I only got it because I wanted something that would fit in a
low-capacity 2D-barcode. After all, I've got my personal brand to think about!
The old z-dawg nickname from my WoW days. That's what I want people's URL fields
to say when they browse this site. My initial thought on how I should achieve
this was that there should be some JavaScript API that let's me modify the
contents of the URL bar. But I quickly realized that that is probably one of the
most sought-after browsers features by phishers and other cybercrims and even in
this crazy world, there probably won't be such an API anytime soon.

Sooo... How should I achieve it then? Redirection of course! But how? Upgrade to
HTTPS immediately? Or redirect first? Manage certs for the redirecting domain?
Or don't bother with that? And what about the "www" subdomain? Is that still
cool to have? Let's look at what the pros are doing!

## The Many Names of the Element Matrix Client
Now, I'm a fan of the Matrix messaging protocol. The premier client in the
Matrix ecosystem is (at the time of writing) called Element. However, that
hasn't always been the case. It used to be called Riot and before that, Vector.
So, what happens when you try to use the old URLs for the project?

|URL              |"Location" Response Header|Status Code|Note                     |
|-----------------|--------------------------|-----------|-------------------------|
|http://vector.im |https://element.io/about  |301        |advertises HTTP/3 support|
|https://vector.im|https://element.io/about  |301        |advertises HTTP/3 support|
|http://riot.im   |https://riot.im           |301        |                         |
|https://riot.im  |https://element.io/about  |301        |                         |

So, "vector.im" sends you directly to the new domain, "element.io". With an
encrypted connection too! But "riot.im" upgrades to an encrypted connection
first, which then sends you to the new domain. Eh, ok? A little strange that
they work differently: using plain HTTP for "riot.im" adds an extra redirect.
Also, responses from "vector.im" indicate that HTTP/3 can be used, but the
server we get redirected to says no such thing. Ok look, I love the work that
the Matrix folks are doing but maybe their redirect game is not 100%. Who cares?
Let's look at another example.

## The Misspellings of ~~Gogle~~ ~~Gooogle~~ Google
Google knows everything about us. Including that we sometimes misspell domain
names. For that reason, they have registered some common misspellings to help
guide us to the right place. That is, "https://www.google.com".

|URL                |"Location" Response Header|Status Code|Note                 |
|-------------------|--------------------------|-----------|---------------------|
|http://gogle.com   |https://www.google.com    |301        |                     |
|https://gogle.com  |https://www.google.com    |301        |Provides invalid cert|
|http://gooogle.com |https://www.google.com    |301        |                     |
|https://gooogle.com|https://www.google.com    |301        |Provides invalid cert|

Hmm. Ok, they all redirect to "https://www.google.com", which makes sense. (So
does "https://google.com", by the way.) However, they don't serve a valid TLS
cert for the misspelled domains, which makes the big G's correction effort less
smooth. What they serve instead is a huge cert that covers most of Google's
interests: "google.com", "google.se" and other TLDs, "youtube.com",
"doubleclick.com" and so on. Huge as it is, they didn't bother with putting the
misspelled domain names in there. One might think that what they did instead was
to simply reuse their main cert (despite being invalid for the misspelled
domains), but that's not the case either. Curiously enough, the cert that is
served for the misspelled domains is not identical to the main cert, albeit very
similar in content. So Google's redirect game is perhaps not 100% eiter. What
else do we have?

## The Remnants of the Sprint/T-Mobile Merger
I feel right at home here, in the telco sector. Surely, some of these
noble and gentle \*ahem\* giants know what they're doing. For instance, what
happened after the Sprint/T-Mobile merger a few years ago? Did they set up their
redirects for a smooth transition?

|URL                   |"Location" Response Header                |Status Code|Note                                |
|----------------------|------------------------------------------|-----------|------------------------------------|
|http://sprint.com     |-                                         |503        |Service unavailable                 |
|https://sprint.com    |-                                         |503        |Service unavailable, TLS cert valid |
|http://www.sprint.com |https://www.t-mobile.com/?src=spr&rdpage=/|301        |                                    |
|https://www.sprint.com|https://www.t-mobile.com/                 |302        |Different cert than "sprint.com"    |

Ehhh, what? So the main 2LD for Sprint gets a 503? However, Wayback Machine
suggests that it's supposed to redirect to "https://www.t-mobile.com/", but that
they seem to be experiencing some intermittent errors as of late. It looks like
they are using a dedicated cert for the redirect, judging by its "Common Name"
field (the TLS connection works just fine). But it's a different cert than what
we get when we're visiting "https://www.sprint.com". Speaking of, the "www"
subdomain seems to be working ok. But it uses different redirect codes for
encrypted/unencrypted connections. Weird. Also, although the "www" cert is valid
for the main 2LD, it is, as mentioned, different from the one we get. And what's
with the query param gobbledygook? If not the telcos, who is getting this right?

## The Master Himself, Mr. Elon "x.com" Musk
Ok, let's check out the master rebrander himself. \*Types "twitter.com" in
browser\* What!? Twitter doesn't redirect to X? What the hell kind of rebrand
is this? Apparently the redirects go in the opposite direction of Elon's
takeover of the Twitter brand. Fail! Anyways, let's see how he does it.

|URL          |"Location" Response Header|Status Code|Note                      |
|-------------|--------------------------|-----------|--------------------------|
|http://x.com |https://x.com             |301        |                          |
|https://x.com|https://twitter.com       |302        |Same cert as "twitter.com"|

Ahem, so yeah. Different status codes again. Probably what Elon means with the
302 is "moved temporarily". Heh, I wonder just how temporary it will turn out to
be. At least he is not using Twitter's old TLS cert because you get the same for
both "x.com" and "twitter.com" and it's valid for both. Shoot, if he's not
getting it 100% right either, then who is? Maybe I should try to answer some
questions myself and see if I can at least find something that is 100% for me
personally.

## Redirect to desired domain or upgrade to https first? 
This is the first questions I'm considering. Some day I might make an attempt at
a more quantitative examination of this to see how people do it in general. For
now, I'll settle for three examples who all do this differently.

|URL                |"Location" Response Header|Status Code|Note                                  |
|-------------------|--------------------------|-----------|--------------------------------------|
|http://t-mobile.com|https://t-mobile.com/     |301        |Encryption first, then "www" subdomain|
|http://google.com  |http://www.google.com/    |301        |"www" subdomain first, then encryption|
|http://apple.com   |https://www.apple.com/    |301        |Simultaneous "www" and encryption     |

At first glance, immediately redirecting the visitor to an encrypted connection
for the domain you want them to visit might seem like the obvious choice since
it can save the visitor one round trip (as per the Apple scenario above).
However, after some thinking I've decided to go with encrypt first, then
redirect. The reason for this is twofold. Firstly, I have to maintain a cert for
the redirecting domain anyway since I want encrypted connections to that domain
to redirect without any warning prompts. Secondly, I would argue that it is more
secure since if you are connecting to "http://apple.com" you _cannot be sure who
you are actually connecting to_. Hence, it makes sense to first establish some
confidence in that the "apple.com" machine you are connecting to is, in fact,
Apple's machine, before you let it redirect you.

## Different certs for the redirecting domain and target domain or one for both?
Let's look at the URLs studied thus far to see how they are doing it.

|URL                   |"Location" Response Header|Certs    |
|----------------------|--------------------------|---------|
|https://vector.im     |https://element.io/about  |Different|
|https://riot.im       |https://element.io/about  |Different|
|https://gogle.com     |https://www.google.com    |Different|
|https://google.com    |https://www.google.com    |Different|
|https://gooogle.com   |https://www.google.com    |Different|
|https://www.sprint.com|https://www.t-mobile.com/ |Different|
|https://x.com         |https://twitter.com       |Same     |
|https://t-mobile.com  |https://www.t-mobile.com/ |Different|
|https://apple.com     |https://www.apple.com/    |Different|

The majority of the examples we've seen use different certs. X and Twitter use
the same (the cert is also used for the respective "www" subdomains) and
are thus sticking out. As I've noted earlier, the certs for the Google
misspellings is invalid for those domains but valid for "www.google.com". They
are nonetheless different from the cert that is served for www.google.com in
other aspects (such as one using a RSA and the other an EC public key) but not
in terms of the Common Name and Subject Alternate Name fields, which are what
I'm mostly interested in here.

From a security standpoint I would argue that it makes the most sense to use
different certs even though that comes with the added weight of managing more
certs. In some sense, Elon Musk has doubled his attack surface because if the
"x.com" boxes are popped, the "twitter.com" ones will be affected too as anyone
with the cert+key from the "x.com" boxes can impersonate a "twitter.com" box.
The Element people will not have this problem. I'm somewhat surprised by the
results though, because some of the certs, like "google.com", are valid for
_a lot_ of different domains and subdomains. But perhaps security is the motive
for that.

I guess there is one valid case for using the same cert for multiple domains:
having both domains as virtual hosts on the same server (or reverse proxy). Then
you are anyways screwed for both domains in case of a compromise.

## Summary
So what did I end up doing? At the time of writing, this site is hosted by
GitLab Pages. It comes with some sort of redirect feature that I couldn't get to
work easily, so I gave that up. Instead, I just added a vhost to a reverse proxy
I'm using for some other stuff. The DNS record for "z5.nu" points to the proxy,
which then redirects the user to this site, which is currently served under
"www.zluudg.com". I didn't bother with any redirects from "zluudg.com" to the
"www" subdomain because I think that if you really want to see this site, you
have to make the extra effort and type the "www" (or scan
[my key fob]({{ page.previous.url }})).

|URL          |"Location" Response Header|Status Code|Note               |
|-------------|--------------------------|-----------|-------------------|
|http://z5.nu |https://z5.nu             |301        |Encryption first...|
|https://z5.nu|https://www.zluudg.com    |301        |...then redirect   |

The certs for "z5.nu" and "www.zluudg.com" are different, for the reasons
mentioned above. Also, GitLab Pages handles the cert for "www.zluudg.com", so I
don't think I could have a dual-domain cert even if I wanted to as long as I'm
using that.

And that's all I have on redirects, for now.
