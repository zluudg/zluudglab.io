---
layout: post
---

# Bead Codes

In our house, we have various kinds of beads laying about:

![](/assets/bead-codes/beads.jpg)

I thought it would be fun to see whether these could be made into some kind of
2D-barcode. The first idea I had was to make a digital business card
using the smallest variant. The smallest beads came with a 13x13 grid that can
be used as a key fob. Perfect for a digital business card! Just encode a URL to
this site and ta-daah! Only one thing... A 13x13 grid of "modules" cannot
contain a whole lot of string data. Moreover, many 2D-barcode formats seem to
require some sort of "quiet zone" around the actual data modules. The width of
the quiet zone can be something like 2 modules for certain formats. If the quiet
zone is to be included on the bead plate grid, that further reduces the capacity
for encoding the URL. This table summarizes some candidate barcode formats:

|Format      |Data Grid |Quiet Zone |Total Grid  |Capacity      |
|------------|----------|-----------|------------|--------------|
|QR (Ver 1)  |21x21     |4 mod      |29x29       |10-25 letters |
|MicroQR     |11x11     |2 mod      |15x15       |5 letters     |
|Aztec       |15x15     |0 mod      |15x15       |12 letters    |
|Aztec Rune  |11x11     |0 mod      |11x11       |8 bits        |
|DataMatrix  |12x12     |1 mod      |14x14       |6 letters     |

Note that this table is a simplification; the capacity can vary depending on
what is to be encoded and what is the desired level of error correction. Most
people should be familiar with a Version 1 QR code, but that is completely out
of the question for a 13x13 grid. Then there is the MicroQR format, which
is too big given its quiet zone (although it would be interesting to try if a
1-module zone is enough to satisfy some less picky scanners). Aztec codes have
the desireable property of not requiring a quiet zone, but the two candidates I
found require a bigger grid or have too low capacity, respectively. 

Ultimately, I went for the DataMatrix format. It's a little too big, but I
thought I could get away with it. The DataMatrix format specifies that the upper
edge and the right edge of the data grid should be alternating black and white
modules. This helps the image processor orient itself. Based on this, and on my
non-existing knowledge of image processing, I prioritized having a quiet zone
of all-white modules close to these two edges so that they would stand out as
much as possible. If you know more about how these algorithms work, don't
hesitate to [contact me](/contact) and tell me whether I made the right choice.

So, DataMatrix 12x12 with a slightly decimated quiet zone it is. Since I won't
be able to encode GitLab Pages' (which is my deployment at the time of writing)
default URL in it, I will have to go for a
6-letter domain name instead and rely on GitLab's BYOD functionality. Actually, 
make that a 5-letter name in case I want to try the MicroQR code at some point.
Should be easy enough to think of something... What about "x.com"? Damnit,
taken. And Jon Postel has the other letters, or how was it now? Short domain
names are apparently a hot commodity. Ultimately, I decided to support local
businesses and buy something under ".nu". Luckily, "z5.nu" was available. "z5"
as in z(luudg), get it? If Andreessen Horowitz are allowed do it, then so am I!
(But I don't like them, so I will not pollute this blog with their domain name.)
Using [zint](https://zint.org.uk/), I generated the code and then copied it
(using a pair of tweezers) onto the key fob. Aaand... It works!

![](/assets/bead-codes/finished.jpg)
